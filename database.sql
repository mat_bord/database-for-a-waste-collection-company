-- -- Setto il formato della data a dd/mm/yy


-----
-- ha senso lasciare un codice in contenitore o conviene mettere un nome??
-----

-- manca deposito

--In genere, nei DBMS l’opzione NO ACTION è quella di default
--DELETE cascade fa si che tutta l'enupla sparisca
----------------------------------------------------------------MANCA FARE UPDATE NELLE CHIAVI ESTERNE-------------------------------------------------------------------
DROP TABLE IF EXISTS Scambi;
DROP TABLE IF EXISTS RaccolteServizi;
DROP TABLE IF EXISTS PartecipazioniTurni;
DROP TABLE IF EXISTS Dipendenti;
DROP TABLE IF EXISTS Depositi;
DROP TABLE IF EXISTS TurniReali;
DROP TABLE IF EXISTS Turni;
DROP TABLE IF EXISTS RifiutiServizi;
DROP TABLE IF EXISTS UtenzePrivate;
DROP TABLE IF EXISTS UtenzeAttivita;
DROP TABLE IF EXISTS Mezzi;
DROP TABLE IF EXISTS Contenitori;
DROP TABLE IF EXISTS Persone;
DROP TABLE IF EXISTS Sedi;
DROP TABLE IF EXISTS Aziende;

-- Crea la tabella Aziende

CREATE TABLE Aziende (
       P_iva                 VARCHAR(11) PRIMARY KEY,
       Nome		             VARCHAR(50),
       Tipo		             VARCHAR(10) CHECK(Tipo='Ente' or Tipo='Attivita')
) ENGINE=InnoDB;


--Crea la tabella Sedi

CREATE TABLE Sedi (
        Azienda                 VARCHAR(11),
        Numero_progressivo      INT(10),
        Nome                    VARCHAR(100),
        PRIMARY KEY (Azienda, Numero_progressivo),
        FOREIGN KEY (Azienda) REFERENCES Aziende(P_iva) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE= InnoDB;


--Crea la tabella Persone

CREATE TABLE Persone (
       Cf               CHAR(16) PRIMARY KEY,
       Nome             VARCHAR(20),
	   Cognome	        VARCHAR(20),
	   Data_nascita 	DATE,
	   Sesso	CHAR(1) CHECK(Sesso='M' OR Sesso='F')
) ENGINE=InnoDB;


--Crea la tabella contenitore
CREATE TABLE Contenitori (
       Codice	          VARCHAR(20) PRIMARY KEY,
       Capacita           DECIMAL,
       Descrizione        VARCHAR(250)
) ENGINE=InnoDB;


--Crea le utenze Utenze_private

CREATE TABLE UtenzePrivate (
       Codice	          VARCHAR(20) PRIMARY KEY,
       Cap                CHAR(5),
	   Regione            VARCHAR(30),
	   Provincia          VARCHAR(20),
	   Paese              VARCHAR(30),
	   Via                VARCHAR(30),
           Numero_civico           VARCHAR(5),
	   Numero_abitanti	  INT DEFAULT 1,
	   Composter	      BOOLEAN,
	   Cf 	              CHAR(16),
	   Contenitore 	      VARCHAR(20),
	   Ente		          VARCHAR(14),
	   FOREIGN KEY (Cf) REFERENCES Persone(Cf)
       	                    ON DELETE CASCADE ON UPDATE CASCADE,
	   FOREIGN KEY (Contenitore) REFERENCES Contenitori(Codice)
       	                    ON DELETE SET NULL ON UPDATE CASCADE,
	   FOREIGN KEY (Ente) REFERENCES Aziende(P_iva)
       	                    ON DELETE SET NULL ON UPDATE CASCADE 
) ENGINE=InnoDB;    



-- Crea la tabella Utenze_attivita
CREATE TABLE UtenzeAttivita (
       Codice               VARCHAR(20) PRIMARY KEY,
       Cap                  CHAR(5),
	   Regione              VARCHAR(20),
	   Provincia            VARCHAR(20),
	   Paese                VARCHAR(30),
	   Via                  VARCHAR(30),
           Numero_civico             VARCHAR(5),
	   Superficie	        DECIMAL,
	   Azienda	            VARCHAR(14) NOT NULL,
	   Numero_progressivo	INT(10) NOT NULL,
	   Contenitore		    VARCHAR(20),
	   Ente 	            VARCHAR(14),
	   FOREIGN KEY (Azienda, Numero_progressivo) REFERENCES Sedi(Azienda, Numero_progressivo)
                            ON DELETE CASCADE ON UPDATE CASCADE,
	   FOREIGN KEY (Contenitore) REFERENCES Contenitori(Codice)
                            ON DELETE SET NULL ON UPDATE CASCADE,
	   FOREIGN KEY (Ente) REFERENCES Aziende(P_iva)
                            ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB;



-- Crea la tabella

CREATE TABLE Dipendenti (
       Cf                   CHAR(16) PRIMARY KEY,
       Azienda		        VARCHAR(18),
       Data_assunzione	    DATE,
	   FOREIGN KEY (Cf) REFERENCES Persone(Cf)
                            ON DELETE CASCADE ON UPDATE CASCADE,
	   FOREIGN KEY (Azienda) REFERENCES Aziende(P_iva)
                            ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;


-- Crea la tabella dei Mezzi

CREATE TABLE Mezzi (
        Targa           CHAR(7) PRIMARY KEY,
        Tipo            VARCHAR(30), 
        Proprietario    VARCHAR(14) NOT NULL,
        FOREIGN KEY (Proprietario) REFERENCES Aziende(P_iva)
                            ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;


-- Crea la tabella Turni


CREATE TABLE Turni (
        Id              VARCHAR(10) PRIMARY KEY,
        Orario_inizio   TIME,
        Orario_fine     TIME
) ENGINE=InnoDB;

-- Crea la tabella TurnoReale


CREATE TABLE TurniReali (
        Codice          VARCHAR(10) PRIMARY KEY,
        Data            DATE NOT NULL,
        Orario          VARCHAR(10),
        Mezzo           CHAR(7),
        FOREIGN KEY (Orario) REFERENCES Turni(Id) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (Mezzo) REFERENCES Mezzi(Targa) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB;

--Crea la tabella RifiutiServizi


CREATE TABLE RifiutiServizi (
        Nome            VARCHAR(50) PRIMARY KEY,
        Prezzo          DECIMAL,
        Cadenza         VARCHAR(20) NOT NULL,
        Riciclabile     INT(1)
) ENGINE=InnoDB;



--Crea la tabella Scambi


CREATE TABLE Scambi (
        Turno_reale     VARCHAR(10),    
        Ora             TIME,   
        Mezzo_ricevente CHAR(10),
        Rifiuto         VARCHAR(50), 
        PRIMARY KEY (Turno_reale, Ora),
        FOREIGN KEY (Turno_reale) REFERENCES TurniReali(Codice) ON UPDATE CASCADE ON DELETE CASCADE,
        FOREIGN KEY (Mezzo_ricevente) REFERENCES Mezzi(Targa) ON DELETE SET NULL ON UPDATE CASCADE,
        FOREIGN KEY (Rifiuto) REFERENCES RifiutiServizi(Nome) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB;

--Crea la tabella PartecipazioniTurni



CREATE TABLE PartecipazioniTurni (
        Dipendente      CHAR(16),
        Turno           VARCHAR(14), 
        PRIMARY KEY (Dipendente, Turno),
        FOREIGN KEY (Dipendente)  REFERENCES Dipendenti(Cf) ON UPDATE CASCADE ON DELETE CASCADE,
        FOREIGN KEY (Turno)  REFERENCES TurniReali(Codice) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

-- Crea la tabella RaccolteServizi

CREATE TABLE RaccolteServizi (
        Turno_reale      VARCHAR(10),
        Ora             TIME, 
        Utenza_privata  VARCHAR(20) NULL,
        Utenza_attivita VARCHAR(20) NULL,
        Rifiuto         VARCHAR(50),
        PRIMARY KEY (Turno_reale, Ora),
        FOREIGN KEY (Turno_reale)  REFERENCES TurniReali(Codice) ON UPDATE CASCADE ON DELETE CASCADE, 
        FOREIGN KEY (Rifiuto) REFERENCES RifiutiServizi(Nome) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;


--Crea la tabella Depositi

CREATE TABLE Depositi (
        Turno_reale              VARCHAR(10),
        Ora                     TIME,
        Azienda                 VARCHAR(11) NOT NULL,
        Numero_progressivo      INT(10) NOT NULL,
        Peso                    INT(10),
        Rifiuto                 VARCHAR(50),
        PRIMARY KEY(Turno_reale, Ora),
        FOREIGN KEY (Azienda, Numero_progressivo) REFERENCES Sedi(Azienda, Numero_progressivo) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (Turno_reale) REFERENCES TurniReali(Codice) ON UPDATE CASCADE ON DELETE CASCADE,         
        FOREIGN KEY (Rifiuto) REFERENCES RifiutiServizi(Nome) ON DELETE CASCADE ON UPDATE CASCADE
        
) ENGINE= InnoDB;




INSERT INTO Aziende VALUES('12363410155', 'COCA_COLA_HBC_ITALIA_S.R.L.', 'attivita')
,('03629090048', 'FERRERO_COMMERCIALE_ITALIA_S.R.L.', 'attivita')
,('01654010345', 'BARILLA_G._e_R._FRATELLI', 'attivita')
,('02196020263', 'CONTARINA_S.P.A.', 'ente')
,('02707410276', 'TREVISAN_S.P.A.', 'ente')
,('02352570226', 'DOLOMITI_AMBIENTE_S.R.L.', 'ente')
,('00470550013', 'LUIGI_LAVAZZA_S.p.A.', 'attivita')
,('01527840274', 'SAN_BENEDETTO_S.P.A.', 'attivita')
,('00488410010', 'TIM_S.p.A.', 'attivita')
,('11325690151', 'SAMSUNG_ELECTRONICS_ITALIA_S.P.A.', 'attivita')
;



INSERT INTO Persone VALUES('BRCFPP80L15D403X', 'Filippo', 'Baracca', '1980-07-15', 'M')
,('BRBCHR72R43F355R', 'Chiara', 'Barbon', '1972-10-03', 'F')
,('GTRFRC69P25I926P', 'Federico', 'Gottardello', '1969-09-25', 'M')
,('ZNTSRA73B53C005U', 'Sara', 'Zanatta', '1973-02-13', 'F')
,('CRMLSN93T31L225S', 'Alessandro', 'Crema', '1993-12-31', 'M')
,('BRDSFO89L53H841N', 'Sofia', 'Bardin', '1989-07-13', 'F')
,('DRGFNC82P19F999M', 'Francesco', 'Durigon', '1982-09-19', 'M')
,('GTTLSE79D41E280A', 'Elisa', 'Gatto', '1979-04-01', 'F')
,('DSMMLN77H28L074G', 'Emiliano', 'De_Simoni', '1977-06-29', 'M')
,('DLLGLC97E30E134N', 'Gianluca', 'Dalla_Ba', '1997-05-30', 'M')
;



INSERT INTO Contenitori VALUES('1', '3', 'Olio')
,('2', '60', 'Appartamento')
,('3', '120', 'Casa')
,('4', '240', 'Piccola_azienda')
,('5', '660', 'Media_azienda')
,('6', '1000', 'Grande_azienda')
,('7', '1700', 'Grande_azienda')
,('8', '2500', 'Grande_azienda')
;


INSERT INTO RifiutiServizi VALUES('Secco', '0', 'Annuale', '0')
,('Umido', '0', 'Annuale', '1')
,('Carta', '0', 'Annuale', '1')
,('Cartone', '5', 'Utilizzo', '1')
,('Vetro', '0', 'Annuale', '1')
,('Plastica', '0', 'Annuale', '1')
,('Vegetale', '10', 'Utilizzo', '1')
,('Rifiuto_agricolo', '25', 'Utilizzo', '1')
,('Amianto', '100', 'Utilizzo', '0')
,('Cartucce_e_toner', '20', 'Utilizzo', '0')
,('Disinfestazione', '100', 'Utilizzo', '0')
,('Consulenza_tecnica', '20', 'Utilizzo', '2')
,('Evento_ecosostenibile', '200', 'Utilizzo', '2')
,('Officine', '50', 'Utilizzo', '0')
,('Rifiuti_speciali', '20', 'Utilizzo', '0')
,('Rifiuti_sanitari', '30', 'Utilizzo', '0')
,('Sgombero_locali', '200', 'Utilizzo', '0')
;


INSERT INTO Sedi VALUES('12363410155', '1', 'Stabilimento-di-Oricola')
,('12363410155', '2', 'Stabilimento-di-Marcianise')
,('12363410155', '3', 'Stabilimento-di-Rionero')
,('03629090048', '1', 'Stabilimento-di-Alba')
,('03629090048', '2', 'Stabilimento-di-S-Angelo')
,('03629090048', '3', 'Stabilimento-di-Balvano')
,('03629090048', '4', 'Stabilimento-di-Pozzuolo')
,('01654010345', '1', 'Stabilimento-di-Ascoli-Piceno')
,('01654010345', '2', 'Stabilimento-di-Castiglione-delle-Stiviere')
,('01654010345', '3', 'Stibilemento-di-Foggia')
,('01654010345', '4', 'Stabilimento-di-Cremona')
,('02196020263', '1', 'Impianto-di-Lovandina')
,('02196020263', '2', 'Impianto-di-Trevignano')
,('02196020263', '3', 'Impianto-di-Villorba')
,('02707410276', '1', 'Impianto-a-Rodengo-Saiano')
,('02707410276', '2', 'Impianto-a-Nuraminis')
,('02352570226', '1', 'Impianto-a-Maserot')
,('02352570226', '2', 'Discarica-a-Pra-de-Anta')
,('00470550013', '1', 'Stabilimento-a-Torino')
,('00470550013', '2', 'Stabilimento-a-Gattinara')
,('00470550013', '3', 'Stabilimento-a-Pozzilli')
,('01527840274', '1', 'Stabilimento-di-Valle-Reale')
,('01527840274', '2', 'Sede-Legale-a-Scorze')
,('00488410010', '1', 'Negozio-a-Feltre')
,('00488410010', '2', 'Negozio-a-Montebelluna')
,('00488410010', '3', 'Negozio-di-Padova')
,('00488410010', '4', 'Negozio-di-Treviso')
,('11325690151', '1', 'Stabilimento-di-Milano')
,('11325690151', '2', 'Stabilimento-di-Verona')
,('11325690151', '3', 'Stabilimento-di-Napoli')
;


INSERT INTO Mezzi VALUES('CM502VS', 'Vasca', '02352570226')
,('FL734HN', 'Spazzatrice', '02707410276')
,('DT361LA', 'Semirimorchio', '02196020263')
,('AI267VY', 'Vasca', '02707410276')
,('CT264MC', 'Spazzatrice', '02196020263')
,('DI867VG', 'Semirimorchio', '02707410276')
,('EG965NJ', 'Vasca', '02707410276')
,('FR395BH', 'Spazzatrice', '02352570226')
,('GA110AA', 'Semirimorchio', '02352570226')
,('DY638JN', 'Vasca', '02196020263')
;


INSERT INTO UtenzeAttivita VALUES('13', '67063', 'Abruzzo', 'AQ', 'Oricola', 'Immagine', '1', '30000', '12363410155', '1', '7', '02707410276')
,('14', '81025', 'Campania', 'CE', 'Marcianise', 'Giovanni_Francesco_Maggio', '1', '88000', '12363410155', '2', '8', '02707410276')
,('15', '85028', 'Campania', 'PZ', 'Rionero', 'Giovanni_Verdi', '23', '55000', '12363410155', '3', '8', '02196020263')
,('16', '12051', 'Piemonte', 'CN', 'Alba', 'Vivaro', '29', '25000', '03629090048', '1', '6', '02352570226')
,('17', '83054', 'Lombardia', 'AV', 'Sant_Angelo', 'Contrada_Lenze', '13', '15000', '03629090048', '2', '5', '02707410276')
,('18', '85050', 'Campania', 'PZ', 'Balvano', 'Contrada_Potito', '1', '50000', '03629090048', '3', '7', '02707410276')
,('19', '20060', 'Lombardia', 'MI', 'Pozzuolo', 'Pietro_Ferrero', '5', '90000', '03629090048', '4', '8', '02196020263')
,('20', '63100', 'Marche', 'AP', 'Ascoli_Piceno', 'Campolungo', '32', '45000', '01654010345', '1', '4', '02707410276')
,('21', '46043', 'Lombardia', 'MN', 'Castiglione_delle_Stiviere', 'Giuseppe_Mazzini', '56/58', '53000', '01654010345', '2', '3', '02707410276')
,('22', '71122', 'Puglia', 'FG', 'Foggia', 'Strada_Statale', '16', '39000', '01654010345', '3', '4', '02196020263')
,('23', '26100', 'Lombardia', 'CR', 'Cremona', 'de_Berenzani', '12', '78000', '01654010345', '4', '7', '02352570226')
,('24', '23981', 'Piemonte', 'TO', 'Torino', 'Bologna', '32', '23981', '00470550013', '1', '5', '02352570226')
,('25', '13045', 'Piemonte', 'VC', 'Gattinara', 'S_Giuseppe', '3', '37000', '00470550013', '2', '6', '02196020263')
,('26', '86077', 'Molise', 'IS', 'Pozzilli', 'Taverna', '10', '56000', '00470550013', '3', '7', '02707410276')
,('27', '65010', 'Abruzzo', 'PE', 'Valle_Reale', 'Popoli', '12', '23000', '01527840274', '1', '4', '02707410276')
,('28', '30037', 'Veneto', 'VE', 'Scorze', 'Kennedy', '65', '54000', '01527840274', '2', '5', '02196020263')
,('29', '32032', 'Veneto', 'BL', 'Feltre', 'Della_Lana', '18', '150', '00488410010', '1', '3', '02707410276')
,('30', '31044', 'Veneto', 'TV', 'Montebelluna', 'Monte_Grappa', '91', '200', '00488410010', '2', '3', '02707410276')
,('31', '35100', 'Veneto', 'PD', 'Padova', 'S_Canziano', '11', '100', '00488410010', '3', '3', '02196020263')
,('32', '31100', 'Veneto', 'TV', 'Treviso', 'Calmaggiore', '49', '75', '00488410010', '4', '2', '02352570226')
,('33', '20019', 'Lombardia', 'MI', 'Milano', 'Mike_Bongiorno', '9', '43000', '11325690151', '1', '5', '02352570226')
,('34', '37100', 'Veneto', 'VR', 'Verona', 'Vigna', '32', '34000', '11325690151', '2', '4', '02196020263')
,('35', '80017', 'Campania', 'NA', 'Napoli', 'Alessandro_Manzoni', '147', '56000', '11325690151', '3', '6', '02707410276')
;

INSERT INTO UtenzePrivate VALUES('1', '31040', 'Veneto', 'TV', 'Montebelluna', 'Vigna', '14', '4', '1', 'BRCFPP80L15D403X', '2', '02196020263')
,('2', '21017', 'Piemonte', 'CN', 'Neive', 'Barche', '37', '2', '0', 'BRCFPP80L15D403X', '3', '02707410276')
,('3', '24010', 'Lombardia', 'BR', 'Aristide', 'Gasperi', '12', '5', '1', 'BRBCHR72R43F355R', '2', '02352570226')
,('4', '95029', 'Veneto', 'VR', 'Sona', 'Salvatore', '45', '3', '1', 'BRBCHR72R43F355R', '3', '02707410276')
,('5', '60121', 'Lazio', 'RI', 'Posta', 'Privata', '9', '3', '0', 'BRBCHR72R43F355R', '3', '02352570226')
,('6', '12020', 'Sardegna', 'CA', 'Goni', 'Cassano', '56', '1', '1', 'GTRFRC69P25I926P', '2', '02196020263')
,('7', '35020', 'Calabria', 'CS', 'Canna', 'Luzzati', '19', '3', '1', 'ZNTSRA73B53C005U', '2', '02196020263')
,('8', '42023', 'Piemonte', 'NO', 'Pogno', 'S_Giorgio', '1', '6', '0', 'CRMLSN93T31L225S', '3', '02707410276')
,('9', '9013', 'Trentino', 'TN', 'Imer', 'San_Giorgio', '6', '3', '1', 'CRMLSN93T31L225S', '2', '02352570226')
,('10', '59100', 'Campania', 'SA', 'Serre', 'Centro', '5', '1', '1', 'CRMLSN93T31L225S', '3', '02196020263')
,('11', '65010', 'Lombardia', 'SO', 'Mese', 'Ortigara', '34', '2', '1', 'CRMLSN93T31L225S', '2', '02707410276')
,('12', '31044', 'Calabria', 'CS', 'Tarzia', 'Giovanni', '54', '2', '0', 'BRDSFO89L53H841N', '3', '02196020263')
;





INSERT INTO Turni VALUES('1', '5:00:00', '11:00:00')
,('2', '5:00:00', '10:00:00')
,('3', '8:00:00', '13:00:00')
,('4', '8:30:00', '16:30:00')
,('5', '14:00:00', '19:00:00')
,('6', '18:00:00', '23:00:00')
,('7', '15:00:00', '22:00:00')
;

INSERT INTO TurniReali VALUES('1', '2017-01-15', '1', 'CM502VS')
,('2', '2017-01-15', '1', 'DT361LA')
,('3', '2017-01-16', '2', 'FL734HN')
,('4', '2017-01-17', '3', 'AI267VY')
,('5', '2017-01-17', '4', 'CT264MC')
,('6', '2017-01-17', '7', 'CM502VS')
,('7', '2017-01-17', '6', 'DI867VG')
,('8', '2017-01-18', '1', 'EG965NJ')
,('9', '2017-01-18', '4', 'FR395BH')
,('10', '2017-01-18', '5', 'FL734HN')
,('11', '2017-01-18', '4', 'DY638JN')
,('12', '2017-01-19', '2', 'GA110AA')
;



INSERT INTO Depositi VALUES('1', '5:10:15', '02196020263', '1', '300', 'Plastica')
,('1', '5:25:34', '02707410276', '1', '560', 'Carta')
,('1', '6:45:40', '02352570226', '1', '323', 'Umido')
,('1', '7:50:23', '02196020263', '2', '432', 'Vetro')
,('1', '8:56:47', '02707410276', '2', '764', 'Carta')
,('2', '5:09:23', '02352570226', '2', '243', 'Secco')
,('2', '6:49:13', '02196020263', '3', '235', 'Vetro')
,('2', '7:57:43', '02352570226', '1', '644', 'Carta')
,('2', '9:10:49', '02196020263', '1', '876', 'Plastica')
,('2', '9:25:26', '02707410276', '1', '456', 'Plastica')
,('3', '6:43:23', '02352570226', '2', '123', 'Carta')
,('3', '6:52:45', '02352570226', '1', '432', 'Vegetale')
,('3', '6:57:46', '02196020263', '2', '234', 'Umido')
,('3', '7:13:12', '02707410276', '2', '167', 'Vetro')
,('3', '8:24:16', '02352570226', '2', '736', 'Plastica')
,('4', '9:34:23', '02196020263', '3', '128', 'Vetro')
,('4', '10:12:25', '02352570226', '1', '329', 'Plastica')
,('4', '12:34:29', '02707410276', '1', '212', 'Carta')
,('4', '12:58:12', '02352570226', '2', '329', 'Umido')
,('5', '8:10:00', '02196020263', '1', '367', 'Carta')
,('5', '9:10:12', '02707410276', '2', '128', 'Secco')
,('5', '13:43:27', '02352570226', '1', '456', 'Vetro')
,('5', '15:53:15', '02196020263', '2', '532', 'Vetro')
,('5', '16:10:10', '02707410276', '1', '456', 'Umido')
,('6', '17:32:12', '02196020263', '3', '321', 'Carta')
,('6', '18:43:13', '02352570226', '2', '543', 'Vegetale')
,('6', '20:43:12', '02707410276', '2', '456', 'Secco')
,('6', '21:20:54', '02352570226', '1', '432', 'Plastica')
,('6', '21:59:12', '02196020263', '1', '753', 'Carta')
,('7', '19:39:23', '02707410276', '1', '285', 'Secco')
,('7', '20:23:19', '02352570226', '2', '287', 'Vetro')
,('7', '21:45:12', '02196020263', '2', '542', 'Vegetale')
,('7', '21:47:50', '02352570226', '1', '346', 'Carta')
,('7', '21:57:34', '02707410276', '2', '352', 'Plastica')
,('8', '5:30:34', '02196020263', '3', '236', 'Vetro')
,('8', '6:34:54', '02352570226', '2', '753', 'Plastica')
,('8', '7:12:38', '02196020263', '1', '235', 'Cartone')
,('8', '8:12:38', '02707410276', '1', '321', 'Vegetale')
,('8', '9:45:32', '02352570226', '1', '543', 'Carta')
,('9', '9:32:12', '02707410276', '2', '364', 'Plastica')
,('9', '9:43:48', '02196020263', '2', '576', 'Cartone')
,('9', '11:45:34', '02352570226', '2', '434', 'Umido')
,('9', '13:43:12', '02352570226', '1', '2', 'Cartucce_e_toner')
,('9', '15:34:38', '02196020263', '3', '343', 'Vetro')
,('10', '14:23:39', '02352570226', '2', '235', 'Plastica')
,('10', '15:49:29', '02707410276', '1', '236', 'Carta')
,('10', '16:00:00', '02707410276', '2', '643', 'Vetro')
,('10', '16:10:12', '02707410276', '1', '236', 'Secco')
,('10', '16:23:43', '02352570226', '1', '643', 'Umido')
,('11', '8:39:23', '02196020263', '1', '462', 'Rifiuto_agricolo')
,('11', '9:56:12', '02352570226', '2', '345', 'Vegetale')
,('11', '14:45:23', '02707410276', '1', '373', 'Plastica')
,('11', '16:12:36', '02196020263', '2', '12', 'Amianto')
,('11', '16:23:38', '02707410276', '1', '484', 'Carta')
,('12', '5:12:32', '02707410276', '2', '485', 'Sgombero_locali')
,('12', '6:32:44', '02352570226', '2', '223', 'Secco')
,('12', '7:34:46', '02196020263', '3', '123', 'Officine')
,('12', '8:10:23', '02707410276', '1', '332', 'Vetro')
,('12', '9:15:45', '02196020263', '1', '124', 'Secco')
;

INSERT INTO Dipendenti VALUES('BRCFPP80L15D403X', '02196020263', '2000-04-03')
,('BRBCHR72R43F355R', '02196020263', '1998-08-07')
,('GTRFRC69P25I926P', '02196020263', '2001-11-22')
,('ZNTSRA73B53C005U', '02707410276', '1999-12-30')
,('CRMLSN93T31L225S', '02707410276', '2005-11-05')
,('BRDSFO89L53H841N', '02707410276', '2000-10-06')
,('DRGFNC82P19F999M', '02352570226', '2010-08-12')
,('GTTLSE79D41E280A', '02352570226', '2002-04-26')
,('DSMMLN77H28L074G', '02352570226', '2001-12-31')
,('DLLGLC97E30E134N', '02352570226', '2015-03-14')
;



INSERT INTO PartecipazioniTurni VALUES('BRCFPP80L15D403X', '1')
,('BRCFPP80L15D403X', '3')
,('BRCFPP80L15D403X', '4')
,('BRBCHR72R43F355R', '2')
,('BRBCHR72R43F355R', '4')
,('GTRFRC69P25I926P', '3')
,('ZNTSRA73B53C005U', '4')
,('ZNTSRA73B53C005U', '8')
,('ZNTSRA73B53C005U', '12')
,('CRMLSN93T31L225S', '5')
,('BRDSFO89L53H841N', '6')
,('DRGFNC82P19F999M', '7')
,('DRGFNC82P19F999M', '9')
,('DRGFNC82P19F999M', '12')
,('DRGFNC82P19F999M', '3')
,('GTTLSE79D41E280A', '8')
,('DSMMLN77H28L074G', '9')
,('DLLGLC97E30E134N', '10')
,('DLLGLC97E30E134N', '6')
;


INSERT INTO RaccolteServizi VALUES('1', '05:15:15', '1', 'NULL', 'Vetro')
,('1', '05:20:34', '11', 'NULL', 'Plastica')
,('1', '06:20:34', '2', 'NULL', 'Carta')
,('1', '07:20:34', '12', 'NULL', 'Vetro')
,('1', '08:20:34', '7', 'NULL', 'Secco')
,('2', '09:20:34', '3', 'NULL', 'Umido')
,('2', '10:20:34', '10', 'NULL', 'Rifiuto_agricolo')
,('2', '11:20:34', '8', 'NULL', 'Vegetale')
,('2', '12:20:34', '4', 'NULL', 'Plastica')
,('2', '13:20:34', '9', 'NULL', 'Amianto')
,('3', '14:20:34', '5', 'NULL', 'Carta')
,('3', '15:20:34', 'NULL', '13', 'Secco')
,('3', '16:20:34', 'NULL', '14', 'Disinfestazione')
,('3', '17:20:34', '6', 'NULL', 'Consulenza_tecnica')
,('3', '18:20:34', 'NULL', '15', 'Plastica')
,('4', '19:20:34', 'NULL', '16', 'Officine')
,('4', '20:20:34', 'NULL', '17', 'Sgombero_locali')
,('4', '21:20:34', 'NULL', '18', 'Vetro')
,('4', '22:20:34', 'NULL', '19', 'Carta')
,('4', '23:20:34', '4', 'NULL', 'Plastica')
,('5', '00:20:34', 'NULL', '20', 'Plastica')
,('5', '01:20:34', '1', 'NULL', 'Carta')
,('5', '02:20:34', 'NULL', '21', 'Vegetale')
,('5', '03:20:34', 'NULL', '22', 'Umido')
,('5', '04:20:34', '2', 'NULL', 'Vetro')
,('6', '05:20:34', '11', 'NULL', 'Plastica')
,('6', '06:20:34', '12', 'NULL', 'Vetro')
,('6', '07:20:34', '2', 'NULL', 'Plastica')
,('6', '08:20:34', '3', 'NULL', 'Carta')
,('6', '09:20:34', 'NULL', '23', 'Umido')
,('7', '10:20:34', '10', 'NULL', 'Carta')
,('7', '11:20:34', 'NULL', '24', 'Evento_ecosostenibile')
,('7', '12:20:34', 'NULL', '25', 'Rifiuti_sanitari')
,('7', '13:20:34', '5', 'NULL', 'Rifiuto_agricolo')
,('7', '14:20:34', '8', 'NULL', 'Rifiuti_speciali')
,('8', '15:20:34', 'NULL', '26', 'Plastica')
,('8', '16:20:34', '9', 'NULL', 'Carta')
,('8', '17:20:34', '11', 'NULL', 'Umido')
,('8', '18:20:34', '6', 'NULL', 'Vetro')
,('8', '19:20:34', 'NULL', '27', 'Carta')
,('9', '20:20:34', '7', 'NULL', 'Secco')
,('9', '21:20:34', 'NULL', '28', 'Vetro')
,('9', '22:20:34', 'NULL', '29', 'Cartucce_e_toner')
,('9', '23:20:34', 'NULL', '30', 'Vetro')
,('9', '00:20:34', 'NULL', '31', 'Carta')
,('10', '01:20:34', 'NULL', '32', 'Secco')
,('10', '02:20:34', 'NULL', '33', 'Vetro')
,('10', '03:20:34', 'NULL', '34', 'Plastica')
,('10', '04:20:34', 'NULL', '35', 'Carta')
,('10', '05:20:34', 'NULL', '13', 'Umido')
,('11', '06:20:34', 'NULL', '14', 'Vetro')
,('11', '07:20:34', 'NULL', '15', 'Carta')
,('11', '08:20:34', 'NULL', '16', 'Secco')
,('11', '09:20:34', 'NULL', '17', 'Vetro')
,('11', '10:20:34', 'NULL', '18', 'Secco')
,('12', '11:20:34', 'NULL', '19', 'Vetro')
,('12', '12:20:34', 'NULL', '20', 'Plastica')
,('12', '13:20:34', 'NULL', '21', 'Carta')
,('12', '14:20:34', 'NULL', '22', 'Cartone')
,('12', '15:20:34', 'NULL', '23', 'Vetro')
;




INSERT INTO Scambi VALUES('1', '07:34:02', 'DT361LA', 'Secco')
,('2', '10:22:45', 'CM502VS', 'Umido')
,('4', '12:54:11', 'DI867VG', 'Carta')
,('5', '14:12:44', 'CM502VS', 'Vetro')
,('6', '20:20:57', 'DI867VG', 'Plastica')
,('7', '03:34:22', 'CM502VS', 'Vegetale')
,('8', '07:42:30', 'FR395BH', 'Rifiuto_agricolo')
,('9', '12:57:12', 'FL734HN', 'Amianto')
,('10', '15:06:44', 'DY638JN', 'Cartucce_e_toner')
,('11', '10:39:12', 'FR395BH', 'Officine')
,('5', '10:32:43', 'DI867VG', 'Rifiuti_speciali')
,('7', '05:22:54', 'AI267VY', 'Rifiuti_sanitari')
,('9', '16:02:56', 'EG965NJ', 'Sgombero_locali')
,('5', '12:48:34', 'CM502VS', 'Secco')
;








--query per calcolare la tariffa di un cliente privato annualmente (fornendo uno specifico anno e l'id cliente)

--seguiamo le seguenti formule:

/*privati senza composter		somma rifiuti*0.2*n n=componenti	sommarifiuti=100+rifiuti occasionali*nvolte

privati con composter		somma rifiuti*0.2*n-5% n=componenti	sommarifiuti=100+rifiuti occasionali*nvolte
*/

/*
abbiamo tralasciato:
    - NON CONSIDERIAMO IL SECCO
	- NON CONSIDERIAMO LO STATO DEL COMPOSTER NEGLI ANNI PRECEDENTI
*/

DROP FUNCTION IF EXISTS TariffaPrivata;
DELIMITER //
CREATE FUNCTION TariffaPrivata (_Id INT, _Anno INT) RETURNS FLOAT
BEGIN   
DECLARE totale FLOAT DEFAULT 0;

SELECT ((SUM(Prezzo)+100)*0.2*up.Numero_abitanti*(1-(0.05*up.Composter))) INTO totale
FROM UtenzePrivate AS up left join RaccolteServizi AS rs on up.Codice=rs.Utenza_privata
                         left join TurniReali AS tr on rs.Turno_reale=tr.Codice
                         left join RifiutiServizi as r on rs.Rifiuto= r.Nome
WHERE up.Codice=_Id and YEAR(tr.Data)=_Anno
GROUP BY up.Codice;

RETURN totale;
END //
DELIMITER ;

 --ora faccio la query esterna per vedere tutte le tariffe dei clienti privati in un anno, ad esempio 2017, ordinando i risultati in ordine alfabetico
 DROP VIEW IF EXISTS TariffaP;
 CREATE VIEW TariffaP AS 
 SELECT up.Cf, Cognome, Nome, up.Codice, Round(TariffaPrivata(Codice, 2017),2) as Tariffa
 FROM UtenzePrivate up JOIN Persone ON up.Cf=Persone.Cf
 ORDER BY Cognome, Nome, up.Codice;
 

 
 --query e funzione per calcolare la tariffa annuale per le aziende in un determinato anno
 
 --funzione per calcolare la tariffa annuale avendo un'azienda e un anno
 
 DROP FUNCTION IF EXISTS TariffaAzienda;
 DELIMITER //
 CREATE FUNCTION TariffaAzienda(_Id INT, _Anno INT) RETURNS FLOAT
 BEGIN
    DECLARE totale INT;
    SELECT ((SUM(r.Prezzo)+1000)*0.01*sqrt(ua.Superficie)) INTO totale
    FROM UtenzeAttivita AS ua left join RaccolteServizi AS rs on ua.Codice=rs.Utenza_attivita
                              join TurniReali AS tr on rs.Turno_reale=tr.Codice
                              join RifiutiServizi as r on rs.Rifiuto= r.Nome
    WHERE ua.Codice=_Id and YEAR(tr.Data)=_Anno
    GROUP BY ua.Codice;
    RETURN totale;
 END //
 DELIMITER ;
 
 --ora faccio la query esterna per vedere tutte le tariffe dei clienti aziende in un anno, ad esempio 2017, ordinando i risultati in ordine alfabetico
 DROP VIEW IF EXISTS TariffaA;
 CREATE VIEW TariffaA AS
 SELECT a.P_iva, a.Nome as Azienda, s.Nome as Sede, round(TariffaAzienda(Codice, 2017),2) as Tariffa
 FROM UtenzeAttivita ua JOIN Sedi s ON ua.Azienda=s.Azienda and ua.Numero_progressivo=s.Numero_progressivo 
                        JOIN Aziende a ON a.P_iva=s.Azienda
 ORDER BY a.Nome, s.Nome;
 
 --trigger: se stiamo cercando di inserire uno scambio fra un mezzo e se stesso mi causa un errore
 DROP TRIGGER IF EXISTS AutoScambio;
 DELIMITER //
 CREATE TRIGGER AutoScambio 
 BEFORE INSERT ON Scambi
 FOR EACH ROW
 BEGIN
   
    DECLARE m1 VARCHAR(7);
    DECLARE codiceTurno VARCHAR(10);
    SELECT Mezzo INTO m1
    FROM TurniReali tr
    WHERE tr.Codice= new.Turno_reale;
    IF (m1=new.Mezzo_ricevente)
    THEN 
        BEGIN
            SELECT Codice INTO codiceTurno FROM TurniReali LIMIT 1;
            INSERT INTO TurniReali VALUES(codiceTurno, 2000-10-10, NULL, NULL);
        END;
    END IF;    
 
 END //
 DELIMITER ;
 
 --trigger per sistemare le chiavi referenti non possibili in Raccolteservizi
 DROP TRIGGER IF EXISTS T1;
 DELIMITER //
 CREATE TRIGGER T1
 AFTER UPDATE ON UtenzePrivate
 FOR EACH ROW
 BEGIN
    IF(new.Codice<>old.Codice) THEN
        UPDATE RaccolteServizi SET Utenza_privata=new.Codice WHERE Utenza_privata=old.Codice;
    END IF;
 END//
 DELIMITER ;
 
 
 DROP TRIGGER IF EXISTS T2;
 DELIMITER //
 CREATE TRIGGER T2
 AFTER UPDATE ON UtenzeAttivita
 FOR EACH ROW
 BEGIN
    IF(new.Codice<>old.Codice) THEN
        UPDATE RaccolteServizi SET Utenza_attivita=new.Codice WHERE Utenza_attivita=old.Codice;
    END IF;
 END//
 DELIMITER ;
 
 DROP TRIGGER IF EXISTS T3;
 DELIMITER //
 CREATE TRIGGER T3
 AFTER DELETE ON UtenzePrivate
 FOR EACH ROW
 BEGIN
       DELETE FROM RaccolteServizi  WHERE Utenza_privata=old.Codice;
 END//
 DELIMITER ;
 
  DROP TRIGGER IF EXISTS T4;
 DELIMITER //
 CREATE TRIGGER T4
 AFTER DELETE ON UtenzeAttivita
 FOR EACH ROW
 BEGIN
       DELETE FROM RaccolteServizi  WHERE Utenza_attivita=old.Codice;
 END//
 DELIMITER ;
 
 
 --il seguente inserimento causa un errore di inserimento
 --INSERT INTO Scambi  VALUES(1,"10:10:10","CM503VS","Carta");

 --TRIGGER: all'inserimento di una raccolta (stessa cosa anche per un deposito e scambio volendo) se l'orario non rientra nel turno reale corrispondente (operzione fuori turno), allora viene sollevato un errore
 -- per utilizzare meno variabili causo l'errore su turno reale che ha solo una chiave primaria
 DROP TRIGGER IF EXISTS FuoriTurno;
 DELIMITER //
 CREATE TRIGGER FuoriTurno
 BEFORE INSERT ON RaccolteServizi
 FOR EACH ROW
 BEGIN
    DECLARE inizio, fine TIME;
    DECLARE cod  VARCHAR(10);
    
    SELECT Orario_inizio into inizio
    FROM TurniReali tr JOIN Turni t ON tr.Orario= t.Id
    WHERE tr.Codice= new.Turno_reale;
    
    SELECT Orario_fine into fine
    FROM TurniReali tr JOIN Turni t ON tr.Orario= t.Id
    WHERE tr.Codice= new.Turno_reale;
    
    IF(new.Ora<inizio OR new.Ora>fine) 
    THEN
        BEGIN 
            SELECT Codice INTO cod 
            FROM TurniReali
            LIMIT 1;
            INSERT INTO TurniReali VALUES(cod, 2000-10-10, NULL, NULL);
            
        END;       
    END IF;
    
 
 END //
 DELIMITER ;
 
 --il seguente inserimento torna un errore, il turno infatti finisce alle 9:30:00
 --INSERT INTO RaccolteServizi VALUES(1, '12:00:00', NULL,NULL,NULL);
 
 --Calcolare quanti giorni ha lavorato un dipendente in un certo anno

DROP FUNCTION IF EXISTS GiorniLavoro;
DELIMITER //
CREATE FUNCTION GiorniLavoro(_CF VARCHAR(16), _Anno INT)
RETURNS INT
BEGIN
    DECLARE tot INT;
        SELECT COUNT(DISTINCT tr.Data) INTO tot
        FROM Dipendenti AS d JOIN PartecipazioniTurni AS pt ON pt.Dipendente=d.Cf
                    JOIN TurniReali as tr ON pt.Turno=tr.Codice
        WHERE YEAR(tr.Data)=_Anno and d.Cf=_CF
        GROUP BY d.Cf;
    RETURN tot;
END //
DELIMITER ;

DROP VIEW IF EXISTS GiorniL;
CREATE VIEW GiorniL AS
SELECT a.Nome AS Azienda, a.P_iva, d.Cf, p.Nome, p.Cognome, GiorniLavoro(d.Cf, 2017) as GiorniLavorati 
FROM Dipendenti as d join Aziende a on d.Azienda=a.P_iva join Persone p on p.Cf=d.Cf
GROUP BY a.Nome, p.Cognome, p.Nome
;


--Calcolare quante ore ha lavorato un dipendente in un certo mese di un certo anno

DROP FUNCTION IF EXISTS OreLavoro;
DELIMITER //
CREATE FUNCTION OreLavoro(_CF VARCHAR(16), _Anno INT, _Mese INT)
RETURNS INT
BEGIN
    DECLARE tot INT DEFAULT 0;
        SELECT SUM(HOUR(t.Orario_fine)-HOUR(t.Orario_inizio)) INTO tot
        FROM Dipendenti AS d  JOIN PartecipazioniTurni AS pt ON pt.Dipendente=d.Cf
                              JOIN TurniReali AS tr ON pt.Turno=tr.Codice
                              JOIN Turni AS t ON t.Id=tr.Orario
        WHERE YEAR(tr.Data)=_Anno and d.Cf=_CF and EXTRACT(MONTH FROM tr.DATA)=_Mese
        GROUP BY Cf;
        RETURN tot;
END //
DELIMITER ;
  
  
DROP VIEW IF EXISTS Stipendio;
CREATE VIEW Stipendio AS
SELECT a.Nome AS Azienda, a.P_iva, d.Cf, p.Nome, p.Cognome, OreLavoro(d.Cf, 2017, 1) as OreLavoro, (OreLavoro(d.Cf, 2017, 1)*8) as Stipendio
FROM Dipendenti as d join Aziende a on d.Azienda=a.P_iva join Persone p on p.Cf=d.Cf
GROUP BY a.Nome, p.Cognome, p.Nome
;


--la sede e la rispettiva azienda che ospitato più kg di spazzatura l'anno scorso
DROP VIEW IF EXISTS PesoTotale;
CREATE VIEW PesoTotale AS
SELECT s.Azienda, s.Numero_progressivo, a.P_iva, SUM(d.Peso) as Totale
    FROM Depositi d JOIN TurniReali tr on d.Turno_reale=tr.Codice 
                    JOIN Sedi s on s.Azienda= d.Azienda and s.Numero_progressivo= d.Numero_progressivo
                    JOIN Aziende a on a.P_iva= s.Azienda
    WHERE YEAR(tr.Data)=(YEAR(CURDATE())-1)
    GROUP BY s.Azienda, s.Numero_progressivo 
;
--fine

DROP VIEW IF EXISTS SedePesoMassimo;
CREATE VIEW SedePesoMassimo AS
SELECT * 
FROM PesoTotale
WHERE Totale= (SELECT MAX(Totale) FROM PesoTotale)
;


-- query che torna il rapporto rifiuti riciclabili/ rifiuti totali in Italia in un determinato anno

DROP FUNCTION IF EXISTS Rapp;
DELIMITER //
CREATE FUNCTION Rapp(_Anno INT) RETURNS FLOAT
BEGIN 
    DECLARE totale, riciclabili INT;
    
    SELECT SUM(d.Peso) into totale
    FROM Depositi d join RifiutiServizi rs on d.Rifiuto= rs.Nome;
    
    
    SELECT SUM(d.Peso) into riciclabili
    FROM Depositi d join RifiutiServizi r on d.Rifiuto= r.Nome 
                    join TurniReali tr on tr.Codice= d.Turno_reale
    WHERE r.Riciclabile=1;
    
    RETURN (riciclabili/totale)*100;

END //
DELIMITER ;

DROP VIEW IF EXISTS Rapporto;
CREATE VIEW Rapporto AS
SELECT Round(Rapp(2017), 2) as Rapporto_Annuale;
 
 -- funzione che ritorna il rapporto ric/tot delle utenze private. se il tot é zero torna 0
DROP FUNCTION IF EXISTS RappPriv;
DELIMITER //
CREATE FUNCTION RappPriv (_Id INT, _Anno INT) RETURNS FLOAT 
BEGIN 
     DECLARE totale INT DEFAULT 0;
     DECLARE totaleric INT DEFAULT 0;
     
     SELECT COUNT(*) into totale
     FROM UtenzePrivate up join RaccolteServizi rs on rs.Utenza_privata=up.Codice
                           join RifiutiServizi r on r.Nome= rs.Rifiuto
                           join TurniReali tr on tr.Codice= rs.Turno_reale
     WHERE up.Codice=_Id and YEAR(tr.Data)=_Anno and r.Riciclabile<>2
     GROUP BY up.Codice;
     
     SELECT COUNT(*) into totaleric
     FROM UtenzePrivate up join RaccolteServizi rs on rs.Utenza_privata=up.Codice
                           join RifiutiServizi r on r.Nome= rs.Rifiuto
                           join TurniReali tr on tr.Codice= rs.Turno_reale
     WHERE up.Codice=_Id and YEAR(tr.Data)=_Anno and r.Riciclabile=1
     GROUP BY up.Codice;
     
     IF (totale=0) THEN
        RETURN 0;
     ELSE
        RETURN (totaleric/totale)*100;
     END IF;
END //
DELIMITER ;

DROP VIEW IF EXISTS RapportoPrivato;
CREATE VIEW RapportoPrivato AS
SELECT p.Cognome, p.Nome, up.Cf, up.Via, up.Numero_civico, up.Paese, round(RappPriv(up.Codice, 2017), 2) as Rapporto_Privato_Percentuale
FROM UtenzePrivate up join Persone p on p.Cf=up.Cf
;

-- funzione che ritorna il rapporto ric/tot delle utenze attività. se il tot é zero torna 0
DROP FUNCTION IF EXISTS RappAtt;
DELIMITER //
CREATE FUNCTION RappAtt (_Id INT, _Anno INT) RETURNS FLOAT 
BEGIN 
     DECLARE totale INT DEFAULT 0;
     DECLARE totaleric INT DEFAULT 0;
     
     SELECT COUNT(*) into totale
     FROM UtenzeAttivita ua join RaccolteServizi rs on rs.Utenza_attivita=ua.Codice
                            join RifiutiServizi r on r.Nome= rs.Rifiuto
                            join TurniReali tr on tr.Codice= rs.Turno_reale
     WHERE ua.Codice=_Id and YEAR(tr.Data)=_Anno and r.Riciclabile<>2
     GROUP BY ua.Codice;
     
     SELECT COUNT(*) into totaleric
     FROM UtenzeAttivita ua join RaccolteServizi rs on rs.Utenza_attivita=ua.Codice
                           join RifiutiServizi r on r.Nome= rs.Rifiuto
                           join TurniReali tr on tr.Codice= rs.Turno_reale
     WHERE ua.Codice=_Id and YEAR(tr.Data)=_Anno and r.Riciclabile=1
     GROUP BY ua.Codice;
     
     IF (totale=0) THEN
        RETURN 0;
     ELSE
        RETURN (totaleric/totale)*100;
     END IF;
END //
DELIMITER ;

DROP VIEW IF EXISTS RapportoAttivita;
CREATE VIEW RapportoAttivita AS
SELECT   a.Nome, s.Numero_progressivo, ua.Via, ua.Numero_civico, ua.Paese, round(RappAtt(ua.Codice, 2017), 2) as Rapporto_Attivita_Percentuale
FROM UtenzeAttivita ua JOIN Sedi s on ua.Azienda=s.Azienda and s.Numero_progressivo= ua.Numero_progressivo
                       JOIN Aziende a on a.P_iva=s.Azienda
;


--funzione che ritorna il paese che ha il rapporto riciclabile/ totale massimo  
--funzione che ritorrìna tutti i paesi
--torna tutte le raccolte con associato il paese e dati sul rifiuto 
DROP VIEW IF EXISTS Raccolte;
CREATE VIEW Raccolte AS
    SELECT rs.*, ua.Paese, ua.Provincia, ua.Regione, c.Capacita, tr.Data, r.*
    FROM RaccolteServizi rs join UtenzeAttivita ua on rs.Utenza_attivita=ua.Codice
                            join Contenitori c on ua.Contenitore= c.Codice
                            join RifiutiServizi r on rs.Rifiuto= r.Nome
                            join TurniReali tr on tr.Codice= rs.Turno_reale
    UNION 
    SELECT rs.* ,up.Paese, up.Provincia, up.Regione, c.Capacita, tr.Data, r.*
    FROM RaccolteServizi rs join UtenzePrivate up on rs.Utenza_privata= up.Codice
                            join Contenitori c on up.Contenitore= c.Codice
                            join RifiutiServizi r on rs.Rifiuto= r.Nome
                            join TurniReali tr on tr.Codice= rs.Turno_reale
;


    


--torna il rapporto riciclabile/totale
DROP FUNCTION IF EXISTS RappPaese; 
DELIMITER //
CREATE FUNCTION RappPaese(_Paese CHAR(30), _Provincia CHAR(2), _Anno INT) RETURNS FLOAT
BEGIN  
    DECLARE volumeTot INT DEFAULT 0;
    DECLARE volumeRic INT DEFAULT 0;
    
    SELECT SUM(Capacita) into volumeTot
    FROM Raccolte 
    WHERE Paese=_Paese and Provincia=_Provincia and YEAR(Data)=_Anno and Riciclabile<>2;
    
    SELECT SUM(Capacita) into volumeRic
    FROM Raccolte 
    WHERE Paese=_Paese and Provincia=_Provincia and YEAR(Data)=_Anno and Riciclabile=1;
    
    IF (volumeTot=0) THEN
        RETURN 0;
    else
        RETURN volumeRic/volumeTot;
    END IF;
END //
DELIMITER ;

--quey che torna la lista di paesi-provincia
DROP VIEW IF EXISTS Paesi;
CREATE VIEW Paesi AS
    SELECT DISTINCT Paese, Provincia
    FROM UtenzeAttivita
    UNION 
    SELECT DISTINCT Paese, Provincia
    FROM UtenzePrivate
    ; 

 
DROP VIEW IF EXISTS RapportiPaesi;
CREATE VIEW RapportiPaesi AS
    SELECT p.*, round(RappPaese(p.Paese, p.Provincia, 2017),2)*100 as Rapporto_Riciclabile_Percentuale 
    FROM  Paesi p
    ;
    


DROP VIEW IF EXISTS PaeseMax;
CREATE VIEW PaeseMax AS
    SELECT *
    FROM RapportiPaesi
    WHERE Rapporto_Riciclabile_Percentuale=(select MAX(Rapporto_Riciclabile_Percentuale) from RapportiPaesi)
    ;

    
    
--stesso ma per le regioni
DROP VIEW IF EXISTS Regioni;
CREATE VIEW Regioni AS
    SELECT DISTINCT Regione
    FROM UtenzeAttivita
    UNION 
    SELECT DISTINCT Regione
    FROM UtenzePrivate
    ;
    
    
DROP FUNCTION IF EXISTS RappReg; 
DELIMITER //
CREATE FUNCTION RappReg(_Regione CHAR(30), _Anno INT) RETURNS FLOAT
BEGIN  
    DECLARE volumeTot INT DEFAULT 0;
    DECLARE volumeRic INT DEFAULT 0;
    
    SELECT SUM(Capacita) into volumeTot
    FROM Raccolte 
    WHERE Regione=_Regione and YEAR(Data)=_Anno and Riciclabile<>2;
    
    SELECT SUM(Capacita) into volumeRic
    FROM Raccolte 
    WHERE Regione=_Regione and YEAR(Data)=_Anno and Riciclabile=1;
    
    IF (volumeTot=0) THEN
        RETURN 0;
    else
        RETURN volumeRic/volumeTot;
    END IF;
END //
DELIMITER ;

DROP VIEW IF EXISTS RapportiRegioni;
CREATE VIEW RapportiRegioni AS
    SELECT r.*, round(RappReg(r.Regione, 2017),2)*100 as Rapporto_Riciclabile_Percentuale 
    FROM  Regioni r
    ;
    
DROP VIEW IF EXISTS RegioneMax;
CREATE VIEW RegioneMax AS
    SELECT *
    FROM RapportiRegioni
    WHERE Rapporto_Riciclabile_Percentuale=(select MAX(Rapporto_Riciclabile_Percentuale) from RapportiRegioni)
    ;

-- procedura che cancella tutti gli scambi, raccolte, depositi, turni degli operai, che risalgono a più di n anni precedenti
-- CancellaVecchi(4) nel 2018 cancellerebbe tutti i dati relativi dal 2014 (escluso) in giù
DROP PROCEDURE IF EXISTS CancellaVecchi;
DELIMITER //
CREATE PROCEDURE CancellaVecchi(_Anni INT)
BEGIN 
    DELETE FROM TurniReali 
    WHERE YEAR(Data)<(YEAR(CURDATE())-_Anni);
END //
DELIMITER ;



